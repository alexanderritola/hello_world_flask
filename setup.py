setup(
  name='Hello World',
  version='0.1',
  description='A hello world flask blueprint with 100% test coverage',
  author='Alexander Ritola',
  author_email='alexanderritola@gmail.com',
  license='MIT',
  packages=find_packages(exclude=('tests', 'tests.*')),
  include_package_data=True,
  zip_safe=False)
